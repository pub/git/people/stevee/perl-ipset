package IPSet;

use 5.028001;
use strict;
use warnings;

require Exporter;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Location ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw() ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw();

our $VERSION = '0.01';

require XSLoader;
XSLoader::load('IPSet', $VERSION);

# Preloaded methods go here.
sub get_set_data($$) {
	my ($session, $setname) = @_;

	# Hash to store and access the set data.
	my %data = ();

	# Array to store and access the set members.
	my @members = ();

	# Call XS function to get the set data.
	my $data_raw = &get_set_data_raw($session, $setname);

	# Split the data from the members part.
	my ($data, $members) = split("Members:", $data_raw);

	# Cut the data part into single pieces for easy parsing.
	my @tmp = split("\n", $data);
	my $header;

	# Extract and assign values to the data hash.
	foreach (@tmp) {
		$data{'name'} = $1 if($_ =~ /Name: (.*)/);
		$data{'type'} = $1 if($_ =~ /Type: (.*)/);
		$data{'revision'} = $1 if ($_ =~ /Revision: (.*)/);
		$data{'memsize'} = $1 if ($_ =~ /memory: (.*)/);
		$data{'references'} = $1 if ($_ =~ /References: (.*)/);
		$data{'entries'} = $1 if ($_ =~ /entries: (.*)/);

		# Grab and store the header.
		$header = $1 if ($_ =~ /Header: (.*)/);
	}

	# Cut the heade string into single pieces.
	@tmp = split(/ /, $header);

	# Add the header values to the data hash.
	$data{'family'} = $tmp[1];
	$data{'hashsize'} = $tmp[3];
	$data{'maxelem'} = $tmp[5];

	# Check if the members part contains data.
	if($members) {
		# Cut the list of members into sigle ones and
		# assign them to the members array.
		@tmp = split("\n", $members);

		foreach (@tmp) {
			# Remove any remain newlines.
			chomp($_);

			# Skip empty elements.
			next unless ($_);

			# Add member to the members array.
			push(@members, $_);
		}

		# Add the members array to the data hash.
		$data{'members'} = \@members;
	}

	# Create object.
	my $self = bless \%data, $setname;
}

#
# Methods to get single data elements.
# (In alphabetical order)
#

# Amount of entries a set has.
sub entries {
	my $self = shift;

	return $self->{entries};
}

# The family of the set.
sub family {
	my $self = shift;

	return $self->{family};
}

# The configured hashsize.
sub hashsize {
	my $self = shift;

	return $self->{hashsize};
}

# A list of current members of the set.
# Stored as array.
sub members {
	my $self = shift;

	return $self->{members};
}

# The amount of maximum elements a set can have.
sub maxelem {
	my $self = shift;

	return $self->{maxelem};
}

# The size of a set in memory.
sub memsize {
	my $self = shift;

	return $self->{memsize};
}

# The name of a set.
sub name {
	my $self = shift;

	return $self->{name};
}

# The amount how often the set is referenced.
sub references {
	my $self = shift;

	return $self->{references};
}

# The revison number of a given set.
sub revision {
	my $self = shift;

	return $self->{revision};
}

# The type of the set.
sub type {
	my $self = shift;

	return $self->{type};
}

1;
__END__

=head1 NAME

perl-ipset - Provides a simple interface to kernel IPSet framework using libipset.

=head1 SYNOPSIS

  use IPSet;

=head1 DESCRIPTION

perl-ipset is a simple interface to the kernel IPset subsystem, provided by the Netfilter framework.

More details on the project website of IPSet: https://ipset.netfilter.org/

=head2 EXPORT

None by default.

=head1 SEE ALSO

https://git.ipfire.org/?p=people/stevee/perl-ipset.git;a=summary

=head1 AUTHOR

Stefan Schantl, stefan.schantl@ipfire.org

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2022 by Stefan Schantl

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.28.1 or,
at your option, any later version of Perl 5 you may have available.


=cut
