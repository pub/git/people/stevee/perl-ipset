#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include <stdio.h>
#include <string.h>

#include <libipset/ipset.h>
#include <libipset/data.h>
#include <libipset/session.h>
#include <libipset/types.h>

/* Private functions */
static int print_outfm_to_string(struct ipset_session *session,
        void *p,
        const char *fmt, ...)
{
        char** output = (char**)p;
        va_list args;
        int r;

	char* buffer = NULL;

        va_start(args, fmt);
        r = vasprintf(&buffer, fmt, args);
        va_end(args);

	if (r < 0)
		goto ERROR;

	// Append
	r = asprintf(output, "%s%s", (output && *output) ? *output : "", buffer);
	if (r < 0)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	if (buffer)
		free(buffer);

        return r;
}

char* call_list_cmd(struct ipset_session *session,
	enum ipset_envopt envopt,
	const char *setname, ...)
{

	enum ipset_cmd cmd = IPSET_CMD_LIST;
	int r;

	char* p = NULL;
	char* sets = NULL;

	// Check if an environment option has been passed.
	if(envopt) {
		// Set the environemt option.
		ipset_envopt_set(session, envopt);
	}

	// Check if a setname has been given.
	if(setname) {
		r = ipset_session_data_set(session, IPSET_SETNAME, setname);
		if (r < 0 ) {
			goto ERROR;
		}
	}

	// Use print_outfm_to_string function to put the output into a string which
	// can be processed further.
	ipset_session_print_outfn(session, print_outfm_to_string, &sets);

	r = ipset_cmd(session, cmd, 0);
	if (r < 0) {
		goto ERROR;
	}

	goto END;

	ERROR:
		// Reset session data.
		ipset_data_reset(ipset_session_data(session));
        END:
		// Remove environment option.
		ipset_envopt_unset(session, envopt);

		// Reset outfn to defaul.
		ipset_session_print_outfn(session, NULL, NULL);

		return sets;
}

MODULE = IPSet		PACKAGE = IPSet

struct ipset_session *
init()
	CODE:
		struct ipset_session *session;

		// Load IPSet types.
		ipset_load_types();

		// Init a new IPSet session
		session = ipset_session_init(NULL, NULL);

		// Abort if the session could not be initialized.
        	if (!session) {
			croak("Cannot initialize ipset session.\n");
		}

		RETVAL = session;
	OUTPUT:
		RETVAL

#
# Functions to directly deal with sets.
#
bool create_set(session, setname, typename, hashsize, maxelem)
		struct ipset_session *session;
		const char *setname;
		const char *typename;
		int hashsize;
		int maxelem;

	PREINIT:
		const struct ipset_type *type;

		// Family current is hardcoded to IPv4.
		int family = NFPROTO_IPV4;

		// The range for bitmap:port based sets currently is hardcoded.
		const uint16_t range_start = 0;
		const uint16_t range_stop = 65535;
	CODE:
		// Load everything
		ipset_load_types();

		// Assin the setname to the session data.
		int r = ipset_session_data_set(session, IPSET_SETNAME, setname);
		if (r < 0) {
			goto ERROR;
		}

		// Assign additinal options to the session data.
		r = ipset_parse_typename(session, IPSET_OPT_TYPE, typename);
		if (r < 0) {
			goto ERROR;
		}
		
		type = ipset_type_get(session, IPSET_CMD_CREATE);
		if (!type) {
			goto ERROR;
		}

		if (strcmp(typename, "bitmap:port") == 0) {
			r = ipset_session_data_set(session, IPSET_OPT_PORT_FROM, &range_start);
			if (r < 0) {
				goto ERROR;
			}

			r = ipset_session_data_set(session, IPSET_OPT_PORT_TO, &range_stop);
			if (r < 0) {
				goto ERROR;
			}
		} else {
			r = ipset_session_data_set(session, IPSET_OPT_FAMILY, &family);
			if (r < 0) {
				goto ERROR;
			}

			r = ipset_session_data_set(session, IPSET_OPT_HASHSIZE, &hashsize);
			if (r < 0) {
				goto ERROR;
			}

			r = ipset_session_data_set(session, IPSET_OPT_MAXELEM, &maxelem);
			if (r < 0) {
				goto ERROR;
			}
		}

		r = ipset_cmd(session, IPSET_CMD_CREATE, 0);
		if (r < 0) {
			goto ERROR;
		}

		RETVAL = true;

		goto END;

		ERROR:
			RETVAL = false;

			// Reset may assigned session data.
			ipset_data_reset(ipset_session_data(session));

		END:
	OUTPUT:
		RETVAL
	

bool
delete_set(session, setname)
	struct ipset_session *session;
	const char *setname;

	PREINIT:
		enum ipset_cmd cmd = IPSET_CMD_DESTROY;

	CODE:
		// Assign the setname to the session data.
		int r = ipset_session_data_set(session, IPSET_SETNAME, setname);
		if (r < 0) {
			goto ERROR;
		}

		r = ipset_cmd(session, cmd, 0);
		if (r < 0) {
			goto ERROR;
		}

		RETVAL = true;

		goto END;

		ERROR:
			RETVAL = false;

			// Reset session data.
			ipset_data_reset(ipset_session_data(session));

		END:
	OUTPUT:
		RETVAL

bool
rename_set(session, setname, new_setname)
	struct ipset_session *session;
	const char *setname;
	const char *new_setname;

	PREINIT:
		enum ipset_cmd cmd = IPSET_CMD_RENAME;

	CODE:
		// Assign the setname to the session data.
		int r = ipset_session_data_set(session, IPSET_SETNAME, setname);
		if (r < 0) {
			goto ERROR;
		}

		r = ipset_session_data_set(session, IPSET_OPT_SETNAME2, new_setname);
		if (r < 0) {
			goto ERROR;
		}

		r = ipset_cmd(session, cmd, 0);
		if (r < 0) {
			goto ERROR;
		}

		RETVAL = true;

		goto END;

		ERROR:
			RETVAL = false;

			// Reset session data.
			ipset_data_reset(ipset_session_data(session));
		END:
	OUTPUT:
		RETVAL


bool
swap_set(session, setname, setname2)
	struct ipset_session *session;
	const char *setname;
	const char *setname2;

	PREINIT:
		enum ipset_cmd cmd = IPSET_CMD_SWAP;

	CODE:
		// Assign the setname to the session data.
		int r = ipset_session_data_set(session, IPSET_SETNAME, setname);
		if (r < 0) {
			goto ERROR;
		}

		// Assign the second set data.
		r = ipset_session_data_set(session, IPSET_OPT_SETNAME2, setname2);
		if (r < 0) {
			goto ERROR;
		}

		r = ipset_cmd(session, cmd, 0);
		if (r < 0) {
			goto ERROR;
		}

		RETVAL = true;

		goto END;

		ERROR:
			RETVAL = false;

			// Reset session data.
			ipset_data_reset(ipset_session_data(session));
		END:
	OUTPUT:
		RETVAL

bool
flush_set(session, setname)
	struct ipset_session *session;
	const char *setname;

	PREINIT:
		enum ipset_cmd cmd = IPSET_CMD_FLUSH;

	CODE:
		// Assign the setname to the session data.
		int r = ipset_session_data_set(session, IPSET_SETNAME, setname);
		if (r < 0) {
			goto ERROR;
		}

		r = ipset_cmd(session, cmd, 0);
		if (r < 0) {
			goto ERROR;
		}

		RETVAL = true;

		goto END;

		ERROR:
			RETVAL = false;

			// Reset session data.
			ipset_data_reset(ipset_session_data(session));
		END:
	OUTPUT:
		RETVAL

bool 
setname_exists(session, setname)
	struct ipset_session *session;
	const char *setname;

	PREINIT:
		enum ipset_cmd cmd = IPSET_CMD_HEADER;

	CODE:
		// Assign the setname as session data.
		int r = ipset_session_data_set(session, IPSET_SETNAME, setname);
		if (r < 0) {
			goto ERROR;
		}

		r = ipset_cmd(session, cmd, 0);
		if (r < 0 ) {
			goto ERROR;
		}
		
		RETVAL = true;

		goto END;

		ERROR:
			RETVAL = false;

			// Reset session data.
			ipset_data_reset(ipset_session_data(session));
		END:
	OUTPUT:
		RETVAL

#
# Functions to add or remove elements to sets.
#
bool
add_address(session, setname, address)
	struct ipset_session *session;
	const char *setname;
	const char *address;

	PREINIT:
		enum ipset_cmd cmd = IPSET_CMD_ADD;
		const struct ipset_type *type;

	CODE:
		// Assign the setname as session data.
		int r = ipset_session_data_set(session, IPSET_SETNAME, setname);
		if (r < 0) {
			goto ERROR;
		}

		// Get the set type.
		type = ipset_type_get(session, cmd);
		if (type == NULL) {
			goto ERROR;
		}

		// Parse and try to add the given address to the session data.
		r = ipset_parse_ip(session, IPSET_OPT_IP, address);
		if (r < 0) {
			goto ERROR;
		}

		r = ipset_cmd(session, cmd, 0);
		if (r < 0) {
			goto ERROR;
		}

		RETVAL = true;

		goto END;

		ERROR:
			RETVAL = false;

			// Reset session data.
			ipset_data_reset(ipset_session_data(session));
		END:
	OUTPUT:
		RETVAL

bool
remove_address(session, setname, address)
	struct ipset_session *session;
	const char *setname;
	const char *address;

	PREINIT:
		enum ipset_cmd cmd = IPSET_CMD_DEL;
		const struct ipset_type *type;

	CODE:
		// Assign setname as session data.
		int r = ipset_session_data_set(session, IPSET_SETNAME, setname);
		if (r < 0) {
			goto ERROR;
		}

		// Get the set type.
		type = ipset_type_get(session, cmd);
		if (type = NULL) {
			goto ERROR;
		}

		// Parse and try to add the given address to the session data.
		r = ipset_parse_ip(session, IPSET_OPT_IP, address);
		if (r < 0) {
			goto ERROR;
		}

		r = ipset_cmd(session, cmd, 0);
		if (r < 0){
			goto ERROR;
		}

		RETVAL = true;

		goto END;

		ERROR:
			RETVAL = false;

			// Reset session data.
			ipset_data_reset(ipset_session_data(session));
		END:
	OUTPUT:
		RETVAL

bool
add_port(session, setname, port)
	struct ipset_session *session;
	const char *setname;
	const char *port;

	PREINIT:
		enum ipset_cmd cmd = IPSET_CMD_ADD;
		const struct ipset_type *type;

	CODE:
		// Assign setname as session data.
		int r = ipset_session_data_set(session, IPSET_SETNAME, setname);
		if (r < 0) {
			goto ERROR;
		}

		// Get the set type.
		type = ipset_type_get(session, cmd);
		if (type == NULL) {
			goto ERROR;
		}

		// Parse and try to add the given port to the session data.
		r = ipset_parse_tcp_udp_port(session, IPSET_OPT_PORT, port);
		if (r < 0) {
			goto ERROR;
		}

		r = ipset_cmd(session, cmd, 0);
		if (r < 0) {
			goto ERROR;
		}

		RETVAL = true;

		goto END;

		ERROR:
			RETVAL = false;

			// Reset session data.
			ipset_data_reset(ipset_session_data(session));
		END:
	OUTPUT:
		RETVAL

bool
remove_port(session, setname, port)
	struct ipset_session *session;
	const char *setname;
	const char *port;

	PREINIT:
		enum ipset_cmd cmd = IPSET_CMD_DEL;
		const struct ipset_type *type;

	CODE:
		// Assign setname to session data.
		int r = ipset_session_data_set(session, IPSET_SETNAME, setname);
		if (r < 0) {
			goto ERROR;
		}

		// Get the set type.
		type = ipset_type_get(session, cmd);
		if (type = NULL) {
			goto ERROR;
		}

		// Parse and add the given port number to the session data.
		r = ipset_parse_tcp_udp_port(session, IPSET_OPT_PORT, port);
		if (r < 0) {
			goto ERROR;
		}

		r = ipset_cmd(session, cmd, 0);
		if (r < 0) {
			goto ERROR;
		}

		RETVAL = true;

		goto END;

		ERROR:
			RETVAL = false;

			// Reset session data.
			ipset_data_reset(ipset_session_data(session));
		END:
	OUTPUT:
		RETVAL

#
# Get functions to ask for various data or receive error messages.
#
void
get_sets(session)
	struct ipset_session *session;

	PREINIT:
		enum ipset_envopt envopt = IPSET_ENV_LIST_SETNAME;

		char* sets = NULL;
		char* p = NULL;

	PPCODE:
		sets = call_list_cmd(session, envopt, NULL);

		char* set = strtok_r(sets, "\n", &p);

		while (set) {
			// Push the set to the perl array.
			XPUSHs(sv_2mortal(newSVpv(set, strlen(set))));

			// Move on to next set
			set = strtok_r(NULL, "\n", &p);
		}

		if(sets) {
			free(sets);
		}

SV*
get_set_data_raw(session, setname)
	struct ipset_session *session;
	char* setname;

	PREINIT:
		enum ipset_envopt envopt;
		char* data = NULL;

	CODE:
		RETVAL = &PL_sv_undef;

		data = call_list_cmd(session, envopt, setname);

		if(data) {
			RETVAL = newSVpv(data, strlen(data));
		}
	OUTPUT:
		RETVAL

SV*
get_error_message(session)
	struct ipset_session *session;

	CODE:
		RETVAL = &PL_sv_undef;

		const char* error = ipset_session_report_msg(session);

		if (error) {
			RETVAL = newSVpv(error, strlen(error));

			// Reset the report buffer.
			ipset_session_report_reset(session);
		}

	OUTPUT:
		RETVAL

#
# Functions to tidy up
#
void
DESTROY(session)
	struct ipset_session *session;

	CODE:
		// Release IPSet session
		ipset_session_fini(session);
