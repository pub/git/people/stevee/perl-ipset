# Before 'make install' is performed this script should be runnable with
# 'make test'. After 'make install' it should work as 'perl Location.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use strict;
use warnings;

use Test::More tests => 27;
BEGIN { use_ok('IPSet') };

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

# Hash with data for the testset.
my %testset = (
	"name" => "TEST",
	"type" => "hash:ip",
	"hashsize" => "1024",
	"maxelem" => "10",
);

my %portset = (
	"name" => "PORTTEST",
	"type" => "bitmap:port",
);

# Array with addresses to be added to the testset.
my @addresses = ("1.1.1.1", "2.2.2.2");

# Array with ports to be added to the portset.
my @ports = ("22", "53", "80", "443");

# Name of the renamed set.
my $setname_renamed = "TEST_RENAMED";

# Init the IPSet module and create a session.
my $session = &IPSet::init();

# Create new IPSet.
my $create_set = &IPSet::create_set($session, $testset{'name'}, $testset{'type'}, $testset{'hashsize'}, $testset{'maxelem'});
ok($create_set, "Sucessfully created set: $testset{'name'}");

# Check if the testset exists.
my $exists = &IPSet::setname_exists($session, $testset{'name'});
ok($exists, "The testset exists.");

# Rename the testset.
my $renamed = &IPSet::rename_set($session, $testset{'name'}, $setname_renamed);
ok($renamed, "Rename successfull.");

# Check if the renamed set exists.
$exists = &IPSet::setname_exists($session, $setname_renamed);
ok($exists, "Renamed testset exists.");

# Create a second set (identical to the testset) for swap testing.
&IPSet::create_set($session, $testset{'name'}, $testset{'type'}, $testset{'hashsize'}, $testset{'maxelem'});
my $swap = &IPSet::swap_set($session, $testset{'name'}, $setname_renamed);
ok($swap, "Successfully swapped sets.");

# Delete the renamed testset.
my $delete = &IPSet::delete_set($session, $setname_renamed);
ok($delete, "Successfully deleted set.");

# Try to grab the data from the testset.
my $data = &IPSet::get_set_data($session, $testset{'name'});
ok($data, "Successfully grabbed set data.");

# Various tests to check the grabbed data.
ok($data->{name} eq $testset{'name'}, "Grabbed setname equals $testset{'name'}");
ok($data->{type} eq $testset{'type'}, "Grabbed type equals $testset{'type'}");
ok($data->{hashsize} eq $testset{'hashsize'}, "Grabbed hashsize equals $testset{'hashsize'}");
ok($data->{maxelem} eq $testset{'maxelem'}, "Grabbed maxelem equals $testset{'maxelem'}");
ok($data->{entries} == 0, "$testset{'name'} should not have any elements now.");
ok($data->{references} == 0, "$testset{'name'} should not be referenced!");

# Get all known sets.
my @sets = &IPSet::get_sets($session);
my $sets_count = @sets;
ok($sets_count ge 1, "At least one set exists yet!");

# Add all addresses from array to the testset.
foreach my $address (@addresses) {
	&IPSet::add_address($session, $testset{'name'}, $address);
}

# Grab set data gain.
$data = &IPSet::get_set_data($session, $testset{'name'});
ok($data->{entries} eq scalar(@addresses), "All addresses added successfully to the set");

# Remove the first address from array from the set.
my $remove = &IPSet::remove_address($session, $testset{'name'}, $addresses[0]);
ok($remove, "Successfully removed $addresses[0] from testset");

# Update set data.
$data = &IPSet::get_set_data($session, $testset{'name'});
ok($data->{entries} lt scalar(@addresses), "Less entries in the set than at the addresses array");
ok($data->{entries} gt 0, "At least one entry on the testset");

# Flush the set.
my $flush = &IPSet::flush_set($session, $testset{'name'});
ok($flush, "Successfully flushed the testset.");

# Update set data.
$data = &IPSet::get_set_data($session, $testset{'name'});
ok($data->{entries} eq 0, "No entries anymore in the testset.");

# CLEANUP: Delete the remaining set.
&IPSet::delete_set($session, $testset{'name'});

# Create the testset for ports.
my $create_port_set = &IPSet::create_set($session, $portset{'name'}, $portset{'type'}, "0", "0");
ok($create_port_set, "Sucessfully created set: $portset{'name'}");

# Check if the testset exists.
$exists = &IPSet::setname_exists($session, $portset{'name'});
ok($exists, "The portset exists.");

# Add all ports from the array to the portset.
foreach my $port (@ports) {
	&IPSet::add_port($session, $portset{'name'}, $port);
}

# Grab set data from portset.
$data = &IPSet::get_set_data($session, $portset{'name'});
ok($data->{entries} eq scalar(@ports), "All ports added successfully to the set");

# Remove a port from the set.
$remove = &IPSet::remove_port($session, $portset{'name'}, $ports[0]);
ok($remove, "Successfully removed $ports[0] from $portset{'name'}");

# Grab set data from portset once again.
$data = &IPSet::get_set_data($session, $portset{'name'});
ok($data->{entries} lt scalar(@ports), "Less entries in the set than at the ports array");
ok($data->{entries} gt 0, "At least one entry on the portset");

# Destroy the port set.
&IPSet::delete_set($session, $portset{'name'});

# Destroy the ipset session.
&IPSet::DESTROY($session);
